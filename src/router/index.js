import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Gridproj from '@/components/Gridproj'
import About from '@/components/About'
import News from '@/components/News'
import Contact from '@/components/Contact'
import Publication from '@/components/Publication'
import Project from '@/components/Project'

Vue.use(Router)

export default new Router({

  routes: [

    {
      path: '/',
      name: 'Gridproj',
      component: Gridproj
    },

    {
      path: '/home',
      name: 'Gridproj',
      component: Gridproj
    },

    {
      path: '/about',
      name: 'About',
      component: About
    },

    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },

    {
      path: '/publication',
      name: 'Publication',
      component: Publication
    },

    {
      path: '/project/:proj',
      name: 'Project',
      component: Project
    }

  ]

})
