// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueAnalytics from 'vue-analytics'
import linkify from 'vue-linkify'

Vue.directive('linkified', linkify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})



Vue.use(VueAnalytics, {
  id: 'UA-65825344-8',
  debug: {
    sendHitTask: process.env.NODE_ENV === 'production'
  },
  router

})
