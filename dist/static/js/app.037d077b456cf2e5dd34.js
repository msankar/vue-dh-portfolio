webpackJsonp([0],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(70)
  __webpack_require__(71)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(28),
  /* template */
  __webpack_require__(138),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-767fa3d8",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = [{"name":"Mark 16","altnm":"mark16","urlp":"https://mark16.vital-it.ch/","imgsrcp":"/static/proj/Mark16_3.jpg","bannersrcp":"","tags":["Edition","Portal","Publishing","Manuscript"],"sourcecodein":"","sourceurl":"","description":"Mark 16 as a test case of a new research model in digital New Testament studies.","imgcaption":"Portrait of Mark in a Gospel According to the Four Evangelists, 1253, Armenia; author: Freer Gallery of Art; © Public Domain, wikicommons","summarytextshort":"MARK16 develops a new research model in digitized biblical sciences, based on a test case found in the New Testament: the last chapter of the Gospel according to Mark. The goal of the project is to create a virtual research environment, that will be the outputs website for textual criticism and exegesis of Mark 16, allowing to compare researchers’ hypotheses. SNF website: http://p3.snf.ch/project-179755","summarytextlong":"","abstracttext":"MARK16 is the first virtual research environment (VRE) focused on a biblical chapter and inaugurates a new research model for the digitized Humanities. The chosen topic is a famous enigma in New Testament studies: certain manuscripts evidences do not have an apparition of the resurrected Jesus at the end of the Gospel according to Mark. Presenting in the historical order the research matter, this VRE will become the outputs place for textual criticism and exegesis of Mark 16, and will allow to visualize the «story of the winners and the loosers», according to the French historian François Hartog’s expression.\n\tThe material available in open access or with copyright, included the audio-visual material, will be presented on the VRE according to a historical scale; a new tool, the interpretation tool, will be created to allow the multimodal comparison of the hypotheses, including this one of the PI: the Gospel according to Mark had a previous ending, lost or destroyed, before the endings transmitted from the 4th century by the manuscripts evidences; the complexity of the textual variants illustrates the Ancient topos of people emotions in front of dead alive apparitions, and shows historically the tensions existing between Christian trends at the 1st century C.E. The project is supported by an international team of researchers: Leif Isaksen, Jennifer Knust, Valérie Nicolet, Laurent Romary, Joseph Verheyden et Peter Williams. This project coins the label Digital New Testament studies and fosters the digital turn of New Testament studies; it supports an academic reading of the Bible at a time in which the European research plays a crucial role in maintaining the social peace between religious communities.","logosrcp":"/static/logos/logo_snf.jpg","newslabel":"","news":[{"date":"24 November 2019","info":"Paper at the Annual SBL Meeting, San Diego, CA","content":"Claire Clivaz, “Enlightening the Diversity of Scholarly Opinions on Multimodal Material : Mark 16 as Test-case”. Paper at the DH session (24.11.2019, 4-6:30 PM, Room 8)","url":"https://www.sbl-site.org/meetings/abstract.aspx?id=50425","videolink":""},{"date":"1 December 2019","info":"CFP open for workshop Virtual Research Environments and Ancient Manuscripts","content":"We invite papers that explore the significance of VREs on the study of manuscript cultures and research in the humanities, especially papers that explore issues related to Early Jewish and Christian Literature, New Testament and Classical Studies. –– Deadline: 1 December 2019 ––","url":"/static/download/CFPworshop2020.pdf","videolink":""},{"date":"10-11 September 2020","info":"WORKSHOP Virtual Research Environments and Ancient Manuscripts","content":"The workshop “Virtual Research Environments and Ancient Manuscripts” (org. Claire Clivaz [SIB, Lausanne] and Garrick Allen [Dublin University]) will be held in Lausanne, 10-11 September 2020.","url":"","videolink":""},{"content":"","url":""},{"content":"","url":""}],"outputs":[{"date":"14 June 2019","info":"A new article accepted to publication","content":"Mina Monier, “GA 304, Theophylact's Commentary and the Ending of Mark”, Filología Neotestamentaria 32 (2019), p. 9-22.","url":"","type":"Paper","dld":"","videolink":""},{"date":"4 July 2019","info":"Rome (Italy). A paper on “Working with Biblical Manuscripts (Textual Criticism) / Gospel of Mark” (Joint session)","content":"Claire Clivaz, “The enigma of Mark16: The role of Contemplating Greek Manuscript Evidence Online.”","url":"https://www.sbl-site.org/meetings/abstract.aspx?id=49284","type":"Paper","dld":"","videolink":""},{"date":"23 May 2019","info":"A new Mark16 eTalk !","content":"Mina Monier, 2019. “GA 304 and the Ending of the Gospel of Mark”, eTalk.","videolink":"https://etalk.sib.swiss/?dir=Mark16_MM_1","type":"Presentation","dld":"","url":""},{"date":"14 March 2019","content":"Claire Clivaz, “The impact of Digital Research: Thinking about the MARK16 Project”, Open Theology 5 (2019/1), p. 1-12 ","url":"https://doi.org/10.1515/opth-2019-0001","type":"Paper","dld":"","videolink":""},{"date":"24th January 2019","info":"Lausanne, SIB virtual computational biology seminars series","content":"MARK16 presentation","url":"","type":"Presentation","dld":"","videolink":"https://youtu.be/9x0mlvDqS4w"},{"date":"29-30 November 2018","info":"Neuchâtel, DARIAH-CH workshop","content":"Claire Clivaz and Martial Sankar, «Mark 16 as a test-case for a new research model in digitized Humanities»","url":"https://www.unine.ch/dariah-ch/home.html","type":"Poster","dld":"/static/download/DARIAH-CHposter2018MARK16.pdf","videolink":"https://cast.unine.ch/permalink/v125acd901e5cet6ctvg/"},{"date":"5-7 November 2018","info":"Paris, Conference PLURITEXT – “Textual plurality in the Bible”","content":"Claire Clivaz, «Mk 16,8 and the manuscripts evidences : listen to the scribal voices»","url":"https://pluritext.sciencesconf.org/","type":"","dld":"","videolink":""},{"date":"4-6 October 2018","info":"Zurich, Annual meeting of the Swiss Society of Theology","content":"Claire Clivaz, «Digital Humanities: towards religions outside the book(s)?»","url":"https://www.theologie.uzh.ch/de/faecher/praktisch/kirchenentwicklung/Veranstaltungen.html#5","type":"Lectures","dld":"","videolink":""},{"date":"15-17 October 2018","info":"Lille, Colloque DHNord 2018","content":"Claire Clivaz, «Penser le lieu de l’écriture digitale»","url":"https://live3.univ-lille3.fr/video-etudes/dhnord-2018-production-preservation-et-diffusion-du-discours-scientifique-donnees-lieux-outils.html?fbclid=IwAR2b3cJbBqxhesNge5lfxTbET6S5QjvLLxfG18TG67kRF9btPxVTsTeint8","type":"Lectures","dld":"","videolink":""},{"date":"2 October 2018","info":"DH master course, PARIS, IPT","content":"Claire Clivaz, «Découvrir les Digital Humanities»","url":"","type":"","dld":"","videolink":""}],"etalkfr":"https://etalk.vital-it.ch/?dir=Mark16_DHFR","etalken":"https://etalk.vital-it.ch/?dir=Mark16_DH","etalkimgsrc":"/static/proj/etalkCover_mark16.jpeg"},{"name":"H2020 DESIR","altnm":"H2020DESIR","urlp":"https://www.dariah.eu/2016/08/12/facing-europes-digital-challenges-dariah-invests-into-a-sustainbale-infrastructure-for-future-research/","imgsrcp":"/static/proj/Dariah_DESIR_logo4_wide3.jpg","bannersrcp":"/static/proj/europe_DARIAH.jpeg","tags":["Portal"],"sourcecodein":"","sourceurl":"","description":"DESIR: Taking the next step to becoming a long-term leader and partner within arts and humanities communities.","imgcaption":"Flags in front of the European Commission building in Brussels; Author: Sébastien Bertrand, CC-BY-2.0, wikicommons","summarytextshort":"Europe has a long and rich tradition as a centre for the arts and humanities. However, the digital transformation poses challenges to the arts and humanities research landscape all over the world. Responding to these challenges the Digital Research Infrastructure for Arts and Humanities (DARIAH) was launched as a pan-European network and research infrastructure. After expansion and consolidation, which involved DARIAH’s inscription on the ESFRI roadmap, DARIAH became a European Research Infrastructure Consortium (ERIC) in August 2014.The DESIR project sets out to strengthen the sustainability of DARIAH and firmly establish it as a long-term leader and partner within arts and humanities communities.","summarytextlong":"Europe has a long and rich tradition as a centre for the arts and humanities. However, the digital transformation poses challenges to the arts and humanities research landscape all over the world. Responding to these challenges the Digital Research Infrastructure for Arts and Humanities (DARIAH) was launched as a pan-European network and research infrastructure. After expansion and consolidation, which involved DARIAH’s inscription on the ESFRI roadmap, DARIAH became a European Research Infrastructure Consortium (ERIC) in August 2014.The DESIR project sets out to strengthen the sustainability of DARIAH and firmly establish it as a long-term leader and partner within arts and humanities communities. By DESIR’s definition, sustainability is an evolving dimensional process, divided into the following challenges: Dissemination, Technology, Robustness, Trust, Education. The DESIR consortium is composed of core DARIAH members, representatives from potential new DARIAH members and external technical experts. It is balanced between the different European regions. The SIB and the UNINE are DESIR partners for Switzerland.","abstracttext":"","logosrcp":"/static/logos/logodesir.jpg","newslabel":"Current","news":[{"date":"5-6 December 2019","info":"Neuchâtel, DARIAH-CH workshop #2","content":"Building on the dynamic initiated during the 2018 DARIAH workshop, a new event will be held in December 2019 at the University of Neuchâtel. Young scholars are invited for this new workshop to present their research in depth and to discuss together methodological, data management and research workflow issues. Scientific committee: Simon Gabay (UNINE) | Berenike Herrmann (UNIBAS) | Tobias Hodel (UZH) | Sara Schulthess (SIB) | Elena Spadini (UNIL) | Toma Tasovac (DARIAH). Organisation: Sara Schulthess (SIB) | Simon Gabay (UNINE)","url":"https://dariah-ch-ws19.sciencesconf.org/","type":"Event"},{"date":"DIMPO","info":"Digital Methods, Practices and Ontologies (DIMPO)","content":"Switzerland has actively participated to the first step of the DIMPO inquiry about the digital particies in Switzerland (information on the ASSH academy about the Swiss National Chapter, with Highlights in English, German and French).A new DIMPO inquiry is in preparation in the DARIAH working group co-lead by Costis Dallas and Maciej Maryl. Website of the WG. In the framework of DESIR SIB, Sara Schulthess is in touch with DIMPO and Thomas Schlag (University of Zürich) to test an inquiry focused on a field.","url":"https://dariahre.hypotheses.org/working-groups/digital-methods-practices-and-ontologies"},{"date":"","info":"","content":"","url":""}],"outputs":[{"date":"24th May 2019","info":"Paris (FR), A LECTURE ON DH PEDAGOGY (DESIR SIB & UNINE PARTNERSHIP), ","content":"« Recherche et cours master en humanités numériques : construire un nouveau lieu de transmission du savoir. » (Université Paris Diderot, bâtiment Olympe de Gouges, salle 830, 10h-12h) Claire Clivaz (SIB, Lausanne) et Simon Gabay (Université de Neuchâtel). Discutante: Anne Vial, Université De Rouen","url":"","dld":"","videolink":""},{"date":"29-30 November 2018","info":"Neuchâtel, DARIAH-CH workshop ","content":"The workshop DARIAH-CH has been an important event in the process of adhesion of Switzerland to the European consortium in Digital Humanities DARIAH, organized in the framework of the H2020 project DESIR. During this workshop, different sessions have offered a platform for Swiss and European protagonists to discuss the main topics related to the development of Digital Humanities. Org. by Matthieu Honegger and Simon Gabay (University of Neuchâtel) ; scientific committee : Claire Clivaz, Swiss Institute of Bioinformatics, Lausanne (SIB) ; Laure Ognois, University of Geneva (UNIGE) ; Gerold Schneider, University of Zurich (UZH); Laurent Romary, Dariah-EU; Gerhard Lauer, University of Basel (UNIBAS).","url":"https://www.unine.ch/dariah-ch/home.html","type":"Event","dld":"","videolink":"https://cast.unine.ch/permalink/v125acd901e5cet6ctvg/"},{"date":"8 November 2018","info":"SIB is a cooperative partner of DARIAH","content":"SIB has become a cooperating partner of DARIAH since the 8 November 2018, and therefore joins eight Swiss universities and academies. Claire Clivaz is SIB DARIAH representative, as Head of the SIB Digital Humanities+ Internal Group. More information on DARIAH website: https://www.dariah.eu/2019/01/09/dariah-welcomes-a-new-cooperating-partner-in-switzerland/","url":"https://www.dariah.eu/2019/01/09/dariah-welcomes-a-new-cooperating-partner-in-switzerland/","type":"News","dld":"","videolink":""}],"etalk":[]},{"name":"The eTalks","altnm":"etalk","urlp":"https://etalk.vital-it.ch/","imgsrcp":"https://etalk.vital-it.ch/../i/p041_3.jpg","bannersrcp":"","tags":["Edition","App","Publishing"],"sourcecodein":"github","sourceurl":"https://github.com/msank/etalk-docker","description":"Online talk edition mixing image, text and sound where each portion can be cited just like any paper-based scientific publication.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"outputs":[],"etalk":[]},{"name":"HumaReC","altnm":"humarec-portal","urlp":"https://humarec.org","imgsrcp":"/static/proj/humarec2.png","bannersrcp":"","tags":["Portal","Publishing","Manuscript"],"sourcecodein":"","sourceurl":"","description":"HumaReC project investigates how Humanities research is reshaped and transformed by the digital rhythm of data production and publication.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"output":[],"outputs":[],"etalk":[]},{"name":"HumaReC-MS Viewer","altnm":"humarec-viewer","urlp":"http://humarec-viewer.vital-it.ch/","imgsrcp":"/static/screencapture-humarec-viewer-vital-it-ch-1502789044542.png","bannersrcp":"","tags":["Viewer","Edition","App","Curation","Manuscript"],"sourcecodein":"github","sourceurl":"https://github.com/humarec/humarec-tei","description":"Digital edition of a Greek-Latin-Arabic manuscript of the New Testament in open access and in open source, including studying tools.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"outputs":[],"etalk":[]},{"name":"HumaReC Webbook","altnm":"humarec-webbook","urlp":"https://humarec.org/webbook/book/index.html","imgsrcp":"/static/webbook_image_new.jpg","bannersrcp":"","tags":["Edition"],"sourcecodein":"","sourceurl":"","description":"Research results of the HumaReC project is gathered in a webbook, a new publication format (in collaboration with Brill publishers.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"outputs":[],"etalk":[]},{"name":"Tarsian","altnm":"tarsian","urlp":"https://tarsian.vital-it.ch/about/","imgsrcp":"https://tarsian.vital-it.ch/about/img/baniere3.png","bannersrcp":"","tags":["Viewer","Curation","Edition","App","Manuscript"],"sourcecodein":"github","sourceurl":"https://github.com/msank/tarsian-tei","description":"Digital edition of an ancient Arabic manuscript of the New Testament in open access and in open source.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"outputs":[],"etalk":[]},{"name":"#dariahTeach","altnm":"#dariahTeach","urlp":"https://www.dariah.eu/teach","imgsrcp":"/static/proj/dariahteach_1.jpg","bannersrcp":"","tags":["Portal"],"sourcecodein":"","sourceurl":"","description":"Open-source, high quality, multilingual teaching materials for the digital arts and humanities.","imgcaption":"","summarytextshort":"","summarytextlong":"","abstracttext":"","newslabel":"","news":[],"outputs":[],"etalk":[]}]

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_router__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Hello__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Hello___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_Hello__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_Gridproj__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_Gridproj___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_Gridproj__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_About__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_About___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_About__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_News__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_News___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_News__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_Contact__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_Contact___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_Contact__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_Publication__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_Publication___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__components_Publication__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_Project__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_Project___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__components_Project__);










__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (new __WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]({

  routes: [{
    path: '/',
    name: 'Gridproj',
    component: __WEBPACK_IMPORTED_MODULE_3__components_Gridproj___default.a
  }, {
    path: '/home',
    name: 'Gridproj',
    component: __WEBPACK_IMPORTED_MODULE_3__components_Gridproj___default.a
  }, {
    path: '/about',
    name: 'About',
    component: __WEBPACK_IMPORTED_MODULE_4__components_About___default.a
  }, {
    path: '/contact',
    name: 'Contact',
    component: __WEBPACK_IMPORTED_MODULE_6__components_Contact___default.a
  }, {
    path: '/publication',
    name: 'Publication',
    component: __WEBPACK_IMPORTED_MODULE_7__components_Publication___default.a
  }, {
    path: '/project/:proj',
    name: 'Project',
    component: __WEBPACK_IMPORTED_MODULE_8__components_Project___default.a
  }]

}));

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(48)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(18),
  /* template */
  __webpack_require__(126),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Headermenu_vue__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Headermenu_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Headermenu_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Footersimple_vue__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Footersimple_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_Footersimple_vue__);
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'app',
  components: { Headermenu: __WEBPACK_IMPORTED_MODULE_0__components_Headermenu_vue___default.a, Footersimple: __WEBPACK_IMPORTED_MODULE_1__components_Footersimple_vue___default.a }
});

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Partof_vue__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Partof_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Partof_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Savoirfaire_vue__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Savoirfaire_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Savoirfaire_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Customers_vue__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Customers_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__Customers_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Team_vue__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Team_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__Team_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({

  components: { Imgpanel: __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue___default.a, Partof: __WEBPACK_IMPORTED_MODULE_1__Partof_vue___default.a, Savoirfaire: __WEBPACK_IMPORTED_MODULE_2__Savoirfaire_vue___default.a, Customers: __WEBPACK_IMPORTED_MODULE_3__Customers_vue___default.a, Team: __WEBPACK_IMPORTED_MODULE_4__Team_vue___default.a },

  name: 'about',
  data() {
    return {
      msg: "We help ground-breaking Humanities projects succeed in digital culture.",
      captionmsg: "Tarsian - Studying an ancient 'New Testament' Arabic Manuscript."
    };
  }
});

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'contact',
  data() {
    return {
      msg: "Hello World :)"
    };
  }
});

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'custbox',
  props: ['custcur', 'info'],
  data() {
    return {};
  },

  methods: {

    enter: function () {
      this.$emit('update:info', this.custcur.description);
    }

  }

});

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'customcards',
  props: ['projobj'],
  data() {
    return {
      tagvals: []
    };
  },
  methods: {
    getTags() {
      this.tagvals = this.projobj.tags;
    }
  },

  mounted() {
    this.getTags();
  }

});

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_customers_json__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_customers_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_data_customers_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Custbox_vue__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Custbox_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Custbox_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: { Custbox: __WEBPACK_IMPORTED_MODULE_1__Custbox_vue___default.a },
  name: 'customers',
  data() {
    return {
      contentmsg: "We are fortunate to have great funding supports and collaborations.",
      customers: __WEBPACK_IMPORTED_MODULE_0__assets_data_customers_json___default.a
    };
  }

});

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'footersimple'
});

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Customcards_vue__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Customcards_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Customcards_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_data_taglist_json__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_data_taglist_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__assets_data_taglist_json__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: { Customcards: __WEBPACK_IMPORTED_MODULE_0__Customcards_vue___default.a },

  name: 'gridproj',

  data() {
    return {
      msg: 'Enabling Crosstalk : Digital Humanities x Bioinformatics.',
      a: __WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json___default.a,
      projelt: __WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json___default.a,
      taglist: __WEBPACK_IMPORTED_MODULE_2__assets_data_taglist_json___default.a
    };
  },

  methods: {
    updateMessage() {

      this.$nextTick(function () {
        var smalltag = this.$el.querySelectorAll('.button-small');

        for (var i = 0; i < smalltag.length; i++) {
          if (smalltag[i].textContent == "All Products") {
            smalltag[i].classList.toggle('greenvalid');
          }
        }
      });
    },

    filterGrid(tag, evt) {

      // clear all green valid tag
      var smalltag = this.$el.querySelectorAll('.button-small');
      for (var i = 0; i < smalltag.length; i++) {
        smalltag[i].classList.remove('greenvalid');
      }

      // add green valid

      evt.target.classList.toggle('greenvalid');

      if (evt.target.textContent === "All Products") {
        this.a = this.projelt;
      } else {

        var b = [];
        for (var i = 0; i < this.projelt.length; i++) {
          this.projelt[i].tags.find(function (elt) {
            if (elt == tag) {
              return b.push(__WEBPACK_IMPORTED_MODULE_1__assets_data_projlist_json___default.a[i]);
            }
          });
        }

        this.a = b;
        //this.$set(this.projelt, a )
        console.log(this.a);
      }
    }
  },

  mounted() {
    this.updateMessage();
  }

});

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'headermenu'
});

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'hello',
  data() {
    return {
      msg: 'Welcome to Your Vue.js App'
    };
  }
});

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

  name: 'imgpanel',

  props: ['srcurl', 'captiontext']

});

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'news'
});

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'partof'
});

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'pixbox',
  props: ['mcur'],
  data() {
    return {};
  }

});

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__projectModule_SummaryPart_vue__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__projectModule_SummaryPart_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__projectModule_SummaryPart_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__projectModule_AbstractPart_vue__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__projectModule_AbstractPart_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__projectModule_AbstractPart_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__projectModule_OutputPart_vue__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__projectModule_OutputPart_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__projectModule_OutputPart_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__projectModule_NewsPart_vue__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__projectModule_NewsPart_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__projectModule_NewsPart_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__projectModule_EtalkPart_vue__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__projectModule_EtalkPart_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__projectModule_EtalkPart_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_data_projlist_json__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_data_projlist_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_data_projlist_json__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//










/* harmony default export */ __webpack_exports__["default"] = ({

  components: { Imgpanel: __WEBPACK_IMPORTED_MODULE_0__Imgpanel_vue___default.a, SummaryPart: __WEBPACK_IMPORTED_MODULE_1__projectModule_SummaryPart_vue___default.a, AbstractPart: __WEBPACK_IMPORTED_MODULE_2__projectModule_AbstractPart_vue___default.a, NewsPart: __WEBPACK_IMPORTED_MODULE_4__projectModule_NewsPart_vue___default.a, OutputPart: __WEBPACK_IMPORTED_MODULE_3__projectModule_OutputPart_vue___default.a, EtalkPart: __WEBPACK_IMPORTED_MODULE_5__projectModule_EtalkPart_vue___default.a },

  name: 'project',

  data() {
    return {
      projelt: __WEBPACK_IMPORTED_MODULE_6__assets_data_projlist_json___default.a,
      projcur: 'project',
      imgbanner: null,
      labelnewssection: "News"
    };
  },

  methods: {
    /* method to select the current proj based on altnm */
    selectCurrentPoj(altnm) {

      return this.projelt.filter(function (data) {
        return data.altnm == altnm;
      });
    },
    /* method for simulated scroll behavior to the hash */
    scrollToId(refName) {

      var element = this.$el.querySelectorAll(refName);
      var top = element[0].offsetTop;
      console.log(element);
      window.scrollTo(0, top);
    }

  },

  mounted() {

    this.projcur = this.selectCurrentPoj(this.$route.params.proj)[0];

    this.imgbanner = null;

    if (this.projcur.bannersrcp !== "") {
      this.imgbanner = this.projcur.bannersrcp;
    } else {
      this.imgbanner = this.projcur.imgsrcp;
    }

    this.labelnewssection = "News";
    if (this.projcur.newslabel === "") {
      this.labelnewssection = "News";
    } else {
      this.labelnewssection = this.projcur.newslabel;
    }
  }

});

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_publicationList_json__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_publicationList_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_data_publicationList_json__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'publication',
  data() {
    return {
      msg: "Converting Knowledge into Citable Products.",
      yl: ['2019', '2018', '2017', '2016', '2015'], // year list
      pubs: __WEBPACK_IMPORTED_MODULE_0__assets_data_publicationList_json___default.a,
      cury: '2019',
      curpubs: null
    };
  },

  methods: {

    filterPub(tag, evt) {

      // clear all green valid tag
      var smallY = this.$el.querySelectorAll('.button-small');
      for (var i = 0; i < smallY.length; i++) {
        smallY[i].classList.remove('greenvalid');
      }

      // add green valid

      evt.target.classList.toggle('greenvalid');

      console.log(__WEBPACK_IMPORTED_MODULE_0__assets_data_publicationList_json___default.a);
      console.log(evt.target.textContent);
      this.curpubs = this.pubs[evt.target.textContent];
      console.log(this.curpubs);
    }

  },

  mounted() {
    this.curpubs = this.pubs[this.cury]; // default
    this.$el.querySelectorAll('.button-small')[0].classList.add('greenvalid'); // hardcoded green valide to 2017
  }

});

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_skills_json__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_skills_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_data_skills_json__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'savoirfaire',
  data() {
    return {
      contentmsg: "Supporting Humanities projects with Bioinformatics competences.",
      skills: __WEBPACK_IMPORTED_MODULE_0__assets_data_skills_json___default.a
    };
  }
});

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_members_json__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_data_members_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_data_members_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pixbox_vue__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pixbox_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Pixbox_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'team',
  components: { Pixbox: __WEBPACK_IMPORTED_MODULE_1__Pixbox_vue___default.a },
  data() {
    return {
      titlemsg: "Our Team",
      contentmsg: "A core \"alliage\" of cross-complementary skills coupled with multi-disciplinary backgrounds.",
      members: __WEBPACK_IMPORTED_MODULE_0__assets_data_members_json___default.a
    };
  }

});

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'abstract-part',
  props: ['content'],
  data() {
    return {
      toggle: true,
      str: ""
    };
  },
  methods: {

    // toggleIcon, function that change icon based on toggle switch
    toggleIcon() {

      this.toggle = !this.toggle;

      if (this.toggle === true) {
        this.str = "Read less";
      } else {
        this.str = "Read more";
      }
    }

  },

  mounted() {
    this.toggleIcon();
  }

});

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'etalk-part',

  props: ['urlsrc', 'imgsrc', 'urlsrcfr']
});

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "news-card",
  props: ['newsObj']
});

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__NewsCard_vue__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__NewsCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__NewsCard_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: { NewsCard: __WEBPACK_IMPORTED_MODULE_0__NewsCard_vue___default.a },

  name: 'news-part',

  props: ['newsList', 'sectionTitle']

});

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "output-card",
  props: ['outputObj', 'index']
});

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__OutputCard_vue__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__OutputCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__OutputCard_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: { OutputCard: __WEBPACK_IMPORTED_MODULE_0__OutputCard_vue___default.a },
  name: 'output-part',
  props: ['contentList']
});

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

  name: 'summary-part',

  props: ['contentshort', 'logoPath', 'contentlong'],

  data() {
    return {
      isOpen: false,
      content: this.contentshort,
      str: "full"
    };
  },

  methods: {

    // toggleIcon, function that change icon based on toggle switch
    toggleIcon() {

      this.isOpen = !this.isOpen;
      console.log(this.isOpen);

      if (this.isOpen) {
        this.content = this.contentlong;
        this.str = "short";
      } else {
        this.content = this.contentshort;
        this.str = "full";
      }

      console.log(this.content);
    }

  },

  mounted() {
    console.log(this.isOpen);
    //   // this.toggleIcon();
  }

});

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__App__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__router__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_analytics__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_analytics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_vue_analytics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_linkify__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_linkify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_vue_linkify__);
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.






__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].directive('linkified', __WEBPACK_IMPORTED_MODULE_4_vue_linkify___default.a);

__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].config.productionTip = false;

/* eslint-disable no-new */
new __WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */]({
  el: '#app',
  router: __WEBPACK_IMPORTED_MODULE_2__router__["a" /* default */],
  template: '<App/>',
  components: { App: __WEBPACK_IMPORTED_MODULE_1__App___default.a }
});

__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_analytics___default.a, {
  id: 'UA-65825344-8',
  debug: {
    sendHitTask: "production" === 'production'
  },
  router: __WEBPACK_IMPORTED_MODULE_2__router__["a" /* default */]

});

/***/ }),
/* 44 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 45 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 46 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 47 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 48 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 49 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 50 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 51 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 52 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 53 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 54 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 55 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 56 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 57 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 58 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 59 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 60 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 61 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 62 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 63 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 64 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 65 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 66 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 67 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 68 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 69 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 70 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 71 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 72 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 73 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 74 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 75 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 76 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 77 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 78 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 79 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 80 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 81 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 82 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 83 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 84 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 85 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(78)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(19),
  /* template */
  __webpack_require__(142),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(79)
  __webpack_require__(80)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(20),
  /* template */
  __webpack_require__(143),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(85)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(21),
  /* template */
  __webpack_require__(146),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(62)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(22),
  /* template */
  __webpack_require__(134),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(46)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(23),
  /* template */
  __webpack_require__(124),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(49)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(24),
  /* template */
  __webpack_require__(127),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(81)
  __webpack_require__(5)
  __webpack_require__(5)
  __webpack_require__(82)
  __webpack_require__(83)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(25),
  /* template */
  __webpack_require__(144),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-e41e36d4",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(68)
  __webpack_require__(69)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(26),
  /* template */
  __webpack_require__(137),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-72921b1f",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(58)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(27),
  /* template */
  __webpack_require__(131),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-418ef86f",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(59)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(29),
  /* template */
  __webpack_require__(132),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(63)
  __webpack_require__(64)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(30),
  /* template */
  __webpack_require__(135),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(77)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(31),
  /* template */
  __webpack_require__(141),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(65)
  __webpack_require__(66)
  __webpack_require__(67)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(32),
  /* template */
  __webpack_require__(136),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-5ed441f6",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(47)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(33),
  /* template */
  __webpack_require__(125),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(75)
  __webpack_require__(76)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(34),
  /* template */
  __webpack_require__(140),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(84)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(35),
  /* template */
  __webpack_require__(145),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(55)
  __webpack_require__(56)
  __webpack_require__(54)
  __webpack_require__(57)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(36),
  /* template */
  __webpack_require__(130),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-3df29052",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(44)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(37),
  /* template */
  __webpack_require__(122),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-06ea2fd7",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(45)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(38),
  /* template */
  __webpack_require__(123),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(60)
  __webpack_require__(61)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(39),
  /* template */
  __webpack_require__(133),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-43ca3483",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(50)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(40),
  /* template */
  __webpack_require__(128),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(72)
  __webpack_require__(73)
  __webpack_require__(74)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(41),
  /* template */
  __webpack_require__(139),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-78dae771",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

function injectStyle (ssrContext) {
  __webpack_require__(51)
  __webpack_require__(52)
  __webpack_require__(53)
}
var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(42),
  /* template */
  __webpack_require__(129),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-38dc9d2c",
  /* moduleIdentifier (server only) */
  null
)

module.exports = Component.exports


/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "etalk-part"
  }, [_c('div', {
    staticClass: "etalk-part-container"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-12-24 pure-u-sm-10-24"
  }, [_c('p', {
    staticClass: "title-section"
  }, [_vm._v("Description")]), _vm._v(" "), _c('p', {
    staticClass: "desc-content"
  }, [_vm._v("\n          Click on it !\n        ")]), _vm._v(" "), _c('div', {
    staticClass: "iframe-container"
  }, [_c('a', {
    attrs: {
      "href": _vm.urlsrc,
      "target": "_blank"
    }
  }, [_c('img', {
    attrs: {
      "src": _vm.imgsrc,
      "alt": ""
    }
  })])]), _vm._v(" "), _c('p', {
    staticClass: "desc-content"
  }, [_vm._v("\n            Also available in French "), _c('a', {
    staticClass: "greenvalid",
    attrs: {
      "href": _vm.urlsrcfr
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("video_library")])])])]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  })])])])
},staticRenderFns: []}

/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "news-card "
  }, [_c('p', {
    staticClass: "date-info"
  }, [_vm._v("\n    " + _vm._s(_vm.newsObj.date) + "\n  ")]), _vm._v(" "), _c('p', {
    staticClass: "info"
  }, [_vm._v("\n\n    " + _vm._s(_vm.newsObj.info) + "\n      "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.newsObj.url !== ''),
      expression: "newsObj.url!==''"
    }],
    staticClass: "link"
  }, [_c('a', {
    staticClass: "greenvalid",
    attrs: {
      "href": _vm.newsObj.url,
      "target": "_blank",
      "rel": "noopener noreferrer"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("\n              link\n          ")])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.newsObj.content !== ''),
      expression: "newsObj.content!==''"
    }],
    staticClass: "contribution"
  }, [_c('p', [_vm._v(_vm._s(_vm.newsObj.content))])])])
},staticRenderFns: []}

/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "customers"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 text-content"
  }, [_c('p', {
    staticClass: "title"
  }, [_vm._v("Our Collaborators and Funders")]), _c('p', {
    staticClass: "content"
  }, [_vm._v(_vm._s(_vm.contentmsg))])]), _vm._v(" "), _c('div', {
    staticClass: "logos pure-u-5-6"
  }, [_c('div', {
    staticClass: "custcontainer full center"
  }, _vm._l((_vm.customers), function(c) {
    return _c('div', [_c('custbox', {
      attrs: {
        "custcur": c,
        "info": _vm.contentmsg,
        "defaultinfo": _vm.contentmsg
      },
      on: {
        "update:info": function($event) {
          _vm.contentmsg = $event
        }
      }
    })], 1)
  }), 0)])])])
},staticRenderFns: []}

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "publi"
  }, [_c('div', {
    staticClass: "content-main"
  }, [_c('p', {
    staticClass: "text-box-head"
  }, [_vm._v(_vm._s(_vm.msg))])]), _vm._v(" "), _c('div', {
    staticClass: "yearfilter clearleft"
  }, _vm._l((_vm.yl), function(y) {
    return _c('span', [_c('a', {
      staticClass: "button-small",
      on: {
        "click": function($event) {
          return _vm.filterPub(y, $event)
        }
      }
    }, [_vm._v(_vm._s(y))])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "tracks pure-u-20-24"
  }, _vm._l((_vm.curpubs), function(p) {
    return _c('span', [_c('p', {
      directives: [{
        name: "linkified",
        rawName: "v-linkified:options",
        value: ({
          format: function(value, type) {
            if (type === 'url' && value.length > 10) {
              value = '[link]'
            }
            return value;
          },
          className: 'linkified'
        }),
        expression: "{\n           format: function (value, type) {\n              if (type === 'url' && value.length > 10) {value = '[link]'}\n                return value;},\n            className: 'linkified'\n                }",
        arg: "options"
      }],
      staticClass: "text-format",
      domProps: {
        "innerHTML": _vm._s(p)
      }
    })])
  }), 0)])
},staticRenderFns: []}

/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "app"
    }
  }, [_c('headermenu'), _vm._v(" "), _c('router-view'), _vm._v(" "), _c('footersimple')], 1)
},staticRenderFns: []}

/***/ }),
/* 127 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "footersimple"
  }, [_c('footer', {
    staticClass: "ftsimple-container"
  }, [_c('span', [_c('p', {
    staticClass: "format-footer-p"
  }, [_vm._v("\n\n\n      SIB|Swiss Institute of Bioinformatics 2017\n    ")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "http://www.sib.swiss/"
    }
  }, [_c('img', {
    staticStyle: {
      "height": "35px"
    },
    attrs: {
      "src": "/static/logos/sib_logo_trans_background_low.png",
      "alt": ""
    }
  })])])])])
}]}

/***/ }),
/* 128 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "output-card"
  }, [_c('p', {
    staticClass: "info"
  }, [_vm._v("\n    [" + _vm._s(_vm.index + 1) + "] " + _vm._s(_vm.outputObj.date) + ", " + _vm._s(_vm.outputObj.info) + "\n    "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.outputObj.url !== ''),
      expression: "outputObj.url!==''"
    }],
    staticClass: "link"
  }, [_c('a', {
    staticClass: "greenvalid",
    attrs: {
      "href": _vm.outputObj.url,
      "target": "_blank",
      "rel": "noopener noreferrer"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("\n            link\n        ")])])]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.outputObj.dld !== ''),
      expression: "outputObj.dld!==''"
    }],
    staticClass: "link"
  }, [_c('a', {
    staticClass: "purpleneutral",
    attrs: {
      "href": _vm.outputObj.dld,
      "target": "_blank",
      "rel": "noopener noreferrer"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("\n        save_alt\n      ")])])]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.outputObj.videolink !== ''),
      expression: "outputObj.videolink!==''"
    }],
    staticClass: "link"
  }, [_c('a', {
    staticClass: "orangewarning",
    attrs: {
      "href": _vm.outputObj.videolink,
      "target": "_blank",
      "rel": "noopener noreferrer"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("\n        video_library\n      ")])])])]), _vm._v(" "), _c('div', {
    staticClass: "contribution"
  }, [_c('p', [_vm._v(_vm._s(_vm.outputObj.content))])])])
},staticRenderFns: []}

/***/ }),
/* 129 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "summary-part"
  }, [_c('div', {
    staticClass: "summary-part-container"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-12-24 pure-u-sm-10-24"
  }, [_c('p', {
    staticClass: "title-section"
  }, [_vm._v("Summary "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.contentlong !== ""),
      expression: "contentlong!==\"\""
    }],
    staticClass: "expandsummary greenvalid",
    on: {
      "click": function($event) {
        return _vm.toggleIcon()
      }
    }
  }, [_vm._v(_vm._s(_vm.str))])]), _vm._v(" "), (_vm.isOpen === false) ? _c('p', {
    staticClass: "content"
  }, [_vm._v("\n          " + _vm._s(_vm.contentshort) + "\n        ")]) : _c('p', {
    staticClass: "content"
  }, [_vm._v("\n          " + _vm._s(_vm.contentlong) + "\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-8-24 pure-u-sm-2-24"
  })])])])
},staticRenderFns: []}

/***/ }),
/* 130 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "abstract-part"
  }, [_c('div', {
    staticClass: "abstract-part-container"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-12-24 pure-u-sm-14-24"
  }, [_c('div', {
    staticClass: "toggle-container"
  }, [_c('button', {
    staticClass: "button-small pure-button",
    on: {
      "click": function($event) {
        return _vm.toggleIcon()
      }
    }
  }, [_vm._v(_vm._s(_vm.str))])]), _vm._v(" "), _c('p', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.toggle),
      expression: "toggle"
    }],
    staticClass: "title-section"
  }, [_vm._v("Abstract")]), _vm._v(" "), _c('p', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.toggle),
      expression: "toggle"
    }],
    staticClass: "content"
  }, [_vm._v("\n          " + _vm._s(_vm.content) + "\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  })])])])
},staticRenderFns: []}

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hello"
  }, [_c('h1', [_vm._v(_vm._s(_vm.msg))]), _vm._v(" "), _c('h2', [_vm._v("Essential Links")]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('h2', [_vm._v("Ecosystem")]), _vm._v(" "), _vm._m(1)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', [_c('li', [_c('a', {
    attrs: {
      "href": "https://vuejs.org",
      "target": "_blank"
    }
  }, [_vm._v("Core Docs")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://forum.vuejs.org",
      "target": "_blank"
    }
  }, [_vm._v("Forum")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://gitter.im/vuejs/vue",
      "target": "_blank"
    }
  }, [_vm._v("Gitter Chat")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://twitter.com/vuejs",
      "target": "_blank"
    }
  }, [_vm._v("Twitter")])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vuejs-templates.github.io/webpack/",
      "target": "_blank"
    }
  }, [_vm._v("Docs for This Template")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', [_c('li', [_c('a', {
    attrs: {
      "href": "http://router.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vue-router")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vuex.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vuex")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vue-loader.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vue-loader")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://github.com/vuejs/awesome-vue",
      "target": "_blank"
    }
  }, [_vm._v("awesome-vue")])])])
}]}

/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "news"
  }, [_c('h3', [_vm._v("News")])])
}]}

/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "news-part"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 text-content"
  }, [_c('p', {
    staticClass: "title-section"
  }, [_vm._v(_vm._s(_vm.sectionTitle))])]), _vm._v(" "), _c('div', {
    staticClass: "news-content news"
  }, [_c('div', {
    staticClass: "news-items-container"
  }, [_c('div', {
    staticClass: "news-items full"
  }, _vm._l((_vm.newsList), function(nn) {
    return _c('div', {
      staticClass: "news-item pure-u-lg-1-3 pure-u-md-1-1 pure-u-sm-1-1"
    }, [_c('news-card', {
      attrs: {
        "news-obj": nn
      }
    })], 1)
  }), 0)])])])])
},staticRenderFns: []}

/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "customcards"
  }, [_c('img', {
    attrs: {
      "src": _vm.projobj.imgsrcp,
      "alt": _vm.projobj.altnm
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "title-content"
  }, [(_vm.projobj.summarytextshort !== '') ? _c('p', [_c('router-link', {
    attrs: {
      "to": {
        name: 'Project',
        params: {
          proj: _vm.projobj.altnm
        }
      }
    }
  }, [_vm._v("\n        " + _vm._s(_vm.projobj.name) + "\n      ")])], 1) : _c('p', [_c('a', {
    attrs: {
      "href": _vm.projobj.urlp
    }
  }, [_vm._v("\n        " + _vm._s(_vm.projobj.name) + "\n      ")])])]), _vm._v(" "), _c('div', {
    staticClass: "tag-list-proj"
  }, _vm._l((_vm.tagvals), function(tag) {
    return _c('span', {
      staticClass: "filter-tag"
    }, [_vm._v(_vm._s(tag))])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "overlay"
  }, [_c('p', {
    staticClass: "short-desc"
  }, [_vm._v(_vm._s(_vm.projobj.description))]), _vm._v(" "), _c('div', {
    staticClass: "linkurl"
  }, [_c('span', {
    staticClass: "gotosite"
  }, [_c('a', {
    attrs: {
      "href": _vm.projobj.urlp
    }
  }, [_vm._v("\n        Go to site\n      ")])]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.projobj.summarytextshort !== ''),
      expression: "projobj.summarytextshort!=='' "
    }],
    staticClass: "gotoproject"
  }, [_c('router-link', {
    attrs: {
      "to": {
        name: 'Project',
        params: {
          proj: _vm.projobj.altnm
        }
      }
    }
  }, [_vm._v("\n        View project\n      ")])], 1)])])])
},staticRenderFns: []}

/***/ }),
/* 135 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "partof"
  }, [_c('div', {
    staticClass: "partof-container"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-1-5 pure-u-sm-2-24"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-1-5 pure-u-sm-6-24 logovitalit"
  }, [_c('img', {
    staticStyle: {
      "width": "100%"
    },
    attrs: {
      "src": "/static/logos/sib_logo_medium.jpg",
      "alt": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-2-5 pure-u-sm-14-24"
  }, [_c('p', {
    staticClass: "title-section"
  }, [_vm._v("Our Story")]), _vm._v(" "), _c('p', {
    staticClass: "content"
  }, [_vm._v("\n          Since 2015, Digital Humanities projects have been developed at the SIB, firstly hosted by the Vital-IT group, and have lead to important achievements such as a participation to the H2020 project DESIR lead by the "), _c('a', {
    attrs: {
      "href": "https://www.dariah.eu/"
    }
  }, [_vm._v("ERIC DARIAH")]), _vm._v(",\n          the SNF project "), _c('a', {
    attrs: {
      "href": "https://www.dariah.eu/"
    }
  }, [_vm._v("HumaReC")]), _vm._v(", and the "), _c('a', {
    attrs: {
      "href": "http://p3.snf.ch/project-179755"
    }
  }, [_vm._v("SNF PRIMA grant MARK 16")]), _vm._v(". An internal SIB group has been open on the 1st October 2018 under the lead of Claire Clivaz, Digital Humanities+. It fosters the collaboration between Humanist and Bioinformatics researchers around DH research projects.\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-1-5 pure-u-sm-2-24"
  })])])])
}]}

/***/ }),
/* 136 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "project"
  }, [_c('div', {
    staticClass: "content-main content-main-mark16"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-15-24 pure-u-sm-16-24"
  }, [_c('p', {
    staticClass: "text-box-head"
  }, [_vm._v(" " + _vm._s(_vm.projcur.description))])]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-9-24 pure-u-sm-8-24"
  }, [_c('p', {
    staticStyle: {
      "margin": "60px 90px 0 0"
    }
  }, [_c('img', {
    staticStyle: {
      "height": "105px"
    },
    attrs: {
      "src": _vm.projcur.logosrcp,
      "alt": ""
    }
  })])])])]), _vm._v(" "), _c('div', {
    staticClass: "clearleft"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 gotosite",
    staticStyle: {
      "padding-bottom": "1%"
    }
  }, [_c('span', {}, [_c('a', {
    on: {
      "click": function($event) {
        return _vm.scrollToId('#summary')
      }
    }
  }, [_vm._v("Summary")])]), _vm._v(" "), _c('span', {}, [_c('a', {
    on: {
      "click": function($event) {
        return _vm.scrollToId('#description')
      }
    }
  }, [_vm._v("Description")])]), _vm._v(" "), _c('span', {}, [_c('a', {
    on: {
      "click": function($event) {
        return _vm.scrollToId('#news')
      }
    }
  }, [_vm._v(_vm._s(_vm.labelnewssection))])]), _vm._v(" "), _c('span', {}, [_c('a', {
    on: {
      "click": function($event) {
        return _vm.scrollToId('#output')
      }
    }
  }, [_vm._v("Output")])])])]), _vm._v(" "), _c('div', {
    staticClass: "clearleft"
  }), _vm._v(" "), _c('imgpanel', {
    attrs: {
      "srcurl": _vm.imgbanner,
      "captiontext": _vm.projcur.imgcaption
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 gotosite",
    staticStyle: {
      "padding-top": "4%"
    }
  }, [_c('div', {
    staticClass: "gotosite-part"
  }, [_c('span', {}, [_c('a', {
    attrs: {
      "href": _vm.projcur.urlp
    }
  }, [_vm._v("Go to Site")])])])])]), _vm._v(" "), _c('summary-part', {
    attrs: {
      "id": "summary",
      "contentshort": _vm.projcur.summarytextshort,
      "contentlong": _vm.projcur.summarytextlong,
      "logo-path": _vm.projcur.logosrcp
    }
  }), _vm._v(" "), (_vm.projcur.abstracttext !== '') ? _c('div', [_c('abstract-part', {
    attrs: {
      "content": _vm.projcur.abstracttext
    }
  })], 1) : _vm._e(), _vm._v(" "), _c('div', {
    attrs: {
      "id": "description"
    }
  }, [(_vm.projcur.etalken) ? _c('div', [_c('etalk-part', {
    attrs: {
      "urlsrc": _vm.projcur.etalken,
      "urlsrcfr": _vm.projcur.etalkfr,
      "imgsrc": _vm.projcur.etalkimgsrc
    }
  })], 1) : _vm._e()]), _vm._v(" "), _c('news-part', {
    attrs: {
      "id": "news",
      "news-list": _vm.projcur.news,
      "section-title": _vm.labelnewssection
    }
  }), _vm._v(" "), _c('output-part', {
    attrs: {
      "id": "output",
      "content-list": _vm.projcur.outputs
    }
  })], 1)
},staticRenderFns: []}

/***/ }),
/* 137 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "headermenu"
  }, [_c('div', {
    staticClass: "pure-menu pure-menu-horizontal pure-menu-scrollable"
  }, [_c('ul', {
    staticClass: "pure-menu-list"
  }, [_c('li', {
    staticClass: "pure-menu-item "
  }, [_c('a', {
    staticClass: "pure-menu-link itemdh",
    attrs: {
      "href": "#/home"
    }
  }, [_vm._v("Projects")])]), _vm._v(" "), _c('li', {
    staticClass: "pure-menu-item "
  }, [_c('a', {
    staticClass: "pure-menu-link itemdh",
    attrs: {
      "href": "#/about"
    }
  }, [_vm._v("About")])]), _vm._v(" "), _c('li', {
    staticClass: "pure-menu-item "
  }, [_c('a', {
    staticClass: "pure-menu-link itemdh",
    attrs: {
      "href": "#/publication"
    }
  }, [_vm._v("Publications")])]), _vm._v(" "), _c('li', {
    staticClass: "pure-menu-item "
  }, [_c('a', {
    staticClass: "pure-menu-link itemdh",
    attrs: {
      "href": "#/contact"
    }
  }, [_vm._v("Contact")])])]), _vm._v(" "), _c('a', {
    staticClass: "content-to-hide",
    attrs: {
      "id": "brandlogo",
      "href": "#"
    }
  }, [_vm._v("Digital Humanities +")])]), _vm._v(" "), _c('a', {
    attrs: {
      "id": "siblogo",
      "href": "http://www.sib.swiss/"
    }
  }, [_c('img', {
    staticStyle: {
      "height": "35px"
    },
    attrs: {
      "src": "/static/logos/sib_logo_trans_background_low.png",
      "alt": ""
    }
  })])])
}]}

/***/ }),
/* 138 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "imgpanel"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-5-5 panelfull"
  }, [_c('img', {
    attrs: {
      "src": _vm.srcurl
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "caption-img"
  }, [_c('p', [_vm._v(_vm._s(_vm.captiontext))])])])
},staticRenderFns: []}

/***/ }),
/* 139 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "output-part"
  }, [_c('div', {
    staticClass: "output-part-container"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  }), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-12-24 pure-u-sm-14-24"
  }, [_c('p', {
    staticClass: "title-section"
  }, [_vm._v("Output & Past Events")]), _vm._v(" "), _c('div', _vm._l((_vm.contentList), function(p, i) {
    return _c('span', [_c('output-card', {
      attrs: {
        "output-obj": p,
        "index": i
      }
    })], 1)
  }), 0)]), _vm._v(" "), _c('div', {
    staticClass: "pure-u-lg-6-24 pure-u-sm-2-24"
  })])])])
},staticRenderFns: []}

/***/ }),
/* 140 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "savoirfaire"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 text-content"
  }, [_c('p', {
    staticClass: "title"
  }, [_vm._v("A broad range of savoir-faire")]), _vm._v(" "), _c('p', {
    staticClass: "content"
  }, [_vm._v(_vm._s(_vm.contentmsg))])]), _vm._v(" "), _vm._m(0)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pure-u-sm-1-1 carousel-content carousel"
  }, [_c('div', {
    staticClass: "carousel-items-container"
  }, [_c('ul', {
    staticClass: "carousel-items full"
  }, [_c('li', {
    staticClass: "carousel-item pure-u-lg-1-3 pure-u-sm-1-1"
  }, [_c('div', {
    staticClass: "description"
  }, [_c('p', {
    staticClass: "title-sub-section"
  }, [_vm._v("Technology")])]), _vm._v(" "), _c('div', {
    staticClass: "image-container"
  }, [_c('span', [_c('i', {
    staticClass: "material-icons xl"
  }, [_vm._v("developer_mode")])])]), _vm._v(" "), _c('ul', {
    staticClass: "sub-description-list"
  }, [_c('li', [_c('p', [_vm._v("Web application")])]), _c('li', [_c('p', [_vm._v("Programming")])]), _c('li', [_c('p', [_vm._v("UI/UX")])]), _c('li', [_c('p', [_vm._v("High Performance Computing")])])])]), _vm._v(" "), _c('li', {
    staticClass: "carousel-item pure-u-lg-1-3 pure-u-sm-1-1"
  }, [_c('div', {
    staticClass: "description"
  }, [_c('p', {
    staticClass: "title-sub-section"
  }, [_vm._v("Analytics")])]), _vm._v(" "), _c('div', {
    staticClass: "image-container"
  }, [_c('span', [_c('i', {
    staticClass: "material-icons xl"
  }, [_vm._v("timeline")])])]), _vm._v(" "), _c('ul', {
    staticClass: "sub-description-list content"
  }, [_c('li', [_c('p', [_vm._v("Data science")])]), _c('li', [_c('p', [_vm._v("Reproducibility")])]), _c('li', [_c('p', [_vm._v("Image analysis")])]), _c('li', [_c('p', [_vm._v("Machine learning")])])])]), _vm._v(" "), _c('li', {
    staticClass: "carousel-item pure-u-lg-1-3 pure-u-sm-1-1"
  }, [_c('div', {
    staticClass: "description"
  }, [_c('p', {
    staticClass: "title-sub-section"
  }, [_vm._v("Edition")])]), _vm._v(" "), _c('div', {
    staticClass: "image-container"
  }, [_c('span', [_c('i', {
    staticClass: "material-icons xl"
  }, [_vm._v("chrome_reader_mode")])])]), _vm._v(" "), _c('ul', {
    staticClass: "sub-description-list"
  }, [_c('li', [_c('p', [_vm._v("Manuscript transcription")])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("Multilingual")])]), _vm._v(" "), _c('li', [_c('p', [_vm._v("Copyrights")])]), _c('li', [_c('p', [_vm._v("License")])])])])])])])
}]}

/***/ }),
/* 141 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "pixbox"
  }, [_c('img', {
    attrs: {
      "src": _vm.mcur.pix,
      "alt": _vm.mcur.surname
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "caption"
  }, [_c('p', {
    staticClass: "name"
  }, [_vm._v(_vm._s(_vm.mcur.firstname) + " " + _vm._s(_vm.mcur.surname))]), _vm._v(" "), _c('p', {
    staticClass: "position "
  }, [_vm._v(_vm._s(_vm.mcur.position))])])])
},staticRenderFns: []}

/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "about"
  }, [_c('div', {
    staticClass: "content-main"
  }, [_c('p', {
    staticClass: "text-box-head"
  }, [_vm._v(_vm._s(_vm.msg))])]), _vm._v(" "), _c('div', {
    staticClass: "clearleft"
  }), _vm._v(" "), _c('imgpanel', {
    attrs: {
      "srcurl": "http://tarsian.vital-it.ch/about/img/baniere3.png",
      "captiontext": _vm.captionmsg
    }
  }), _vm._v(" "), _c('partof'), _vm._v(" "), _c('savoirfaire'), _vm._v(" "), _c('team'), _vm._v(" "), _c('customers')], 1)
},staticRenderFns: []}

/***/ }),
/* 143 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact"
  }, [_c('div', {
    staticClass: "content-main"
  }, [_c('p', {
    staticClass: "text-box-head"
  }, [_vm._v(_vm._s(_vm.msg))])]), _vm._v(" "), _c('div', {}), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('div', {})])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row contact-row pure-u-1-1"
  }, [_c('div', {
    staticClass: "form-container pure-u-lg-1-3 pure-u-md-1-2 pure-u-sm-20-24"
  }, [_c('form', {
    staticClass: "pure-form pure-form-aligned",
    attrs: {
      "action": "mailto:martial.sankar@sib.swiss",
      "method": "post",
      "enctype": "text/plain"
    }
  }, [_c('fieldset', [_c('div', {
    staticClass: "item-contact pure-control-group"
  }, [_c('label', {
    attrs: {
      "for": "first-name"
    }
  }, [_vm._v("First Name")]), _vm._v(" "), _c('input', {
    attrs: {
      "id": "first-name",
      "type": "text"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "item-contact pure-control-group"
  }, [_c('label', {
    attrs: {
      "for": "last-name"
    }
  }, [_vm._v("Last Name")]), _vm._v(" "), _c('input', {
    attrs: {
      "id": "last-name",
      "type": "text"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "item-contact pure-control-group"
  }, [_c('label', {
    attrs: {
      "for": "email"
    }
  }, [_vm._v("E-Mail"), _c('sup', [_vm._v("*")])]), _vm._v(" "), _c('input', {
    attrs: {
      "id": "email",
      "type": "email",
      "required": ""
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "item-contact pure-control-group"
  }, [_c('label', {
    attrs: {
      "for": "city"
    }
  }, [_vm._v("Institution")]), _vm._v(" "), _c('input', {
    attrs: {
      "id": "city",
      "type": "text"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "item-contact"
  }, [_c('label', {
    attrs: {
      "for": "message"
    }
  }, [_vm._v("Message"), _c('sup', [_vm._v("*")])]), _vm._v(" "), _c('textarea', {
    staticClass: "pure-u-24-24",
    attrs: {
      "id": "message",
      "required": ""
    }
  })]), _vm._v(" "), _c('button', {
    staticClass: "pure-button-primary pure-button"
  }, [_vm._v("SEND")])])])])])
}]}

/***/ }),
/* 144 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "gridproj"
  }, [_c('div', {
    staticClass: "content-main"
  }, [_c('p', {
    staticClass: "text-box-head"
  }, [_vm._v(_vm._s(_vm.msg))])]), _vm._v(" "), _c('div', {
    staticClass: "filters clearleft pure-u-xl-16-24 pure-u-lg-18-24 pure-u-md-1-1 pure-u-sm-1-1 content-to-hide"
  }, _vm._l((_vm.taglist), function(tag) {
    return _c('span', {
      staticClass: "pure-u-sm-1-2 pure-u-lg-1-8"
    }, [_c('a', {
      staticClass: "button-small",
      on: {
        "click": function($event) {
          return _vm.filterGrid(tag, $event)
        }
      }
    }, [_vm._v(_vm._s(tag))])])
  }), 0), _vm._v(" "), _c('div', {
    staticClass: "pure-g clearleft",
    staticStyle: {
      "padding-top": "80px",
      "padding-right": "8.4%",
      "padding-left": "8.4%"
    }
  }, _vm._l((_vm.a), function(proj) {
    return _c('div', {
      staticClass: "proj-container pure-u-lg-1-2 pure-u-md-1-1"
    }, [_c('customcards', {
      attrs: {
        "projobj": proj
      }
    })], 1)
  }), 0)])
},staticRenderFns: []}

/***/ }),
/* 145 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "team"
  }, [_c('div', {
    staticClass: "pure-g"
  }, [_c('div', {
    staticClass: "pure-u-1-1 text-content"
  }, [_c('p', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.titlemsg))]), _c('p', {
    staticClass: "content"
  }, [_vm._v(_vm._s(_vm.contentmsg))])]), _vm._v(" "), _c('div', {
    staticClass: "pix pure-u-6-6"
  }, [_c('div', {
    staticClass: "pixcontainer full center"
  }, _vm._l((_vm.members), function(mem) {
    return _c('div', [_c('pixbox', {
      attrs: {
        "mcur": mem
      }
    })], 1)
  }), 0)])])])
},staticRenderFns: []}

/***/ }),
/* 146 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "custbox"
  }, [_c('img', {
    attrs: {
      "src": _vm.custcur.imgsrcp,
      "alt": _vm.custcur.altnm
    },
    on: {
      "mouseover": _vm.enter
    }
  })])
},staticRenderFns: []}

/***/ }),
/* 147 */,
/* 148 */,
/* 149 */
/***/ (function(module, exports) {

module.exports = [{"name":"Brill","altnm":"Brill","urlp":"http://www.brill.com/","imgsrcp":"/static/logos/logo_Brill.png","description":"HumaReC webbook is developed in cooperation with Brill publishers."},{"name":"Université de Lausanne","altnm":"UNIL","urlp":"http://www.unil.ch/index.html","imgsrcp":"/static/logos/unilogo_bleu_300dpi.png","description":"Vital-DH is localized at UNIL"},{"name":"Università di Pisa","altnm":"Università di Pisa","urlp":"http://evt.labcd.unipi.it/","imgsrcp":"/static/logos/University_of_Pisa.svg.png","description":"Collaboration with the EVT team, project from University of Pisa led by Prof. Roberto Rosselli Del Turco."},{"name":"Biblioteca Nazionale Marciana","altnm":"Biblioteca Nazionale Marciana","urlp":"https://marciana.venezia.sbn.it/","imgsrcp":"/static/logos/logo_marciana.jpg","description":"Images of HumaReC are the property of the Marciana Library. All rights reserved."},{"name":"University of Innsbruck","altnm":"University of Innsbruck","urlp":"https://transkribus.eu/Transkribus/","imgsrcp":"/static/logos/insbruck_uni.png","description":"We collaborate with the Research Infrastructure Transkribus (funded by the H2020 Project READ - Recognition and Enrichment of Archivial Documents)."},{"name":"ERASMUS+","altnm":"ERASMUS+","urlp":"https://info.erasmusplus.fr/","imgsrcp":"/static/logos/Erasmus-Plus.jpg","description":"Our Founders."},{"name":"SNF","altnm":"Swiss National Science Foundation","urlp":"http://www.snf.ch/fr/Pages/default.aspx","imgsrcp":"/static/logos/logo_snf.jpg","description":"Our Founders."},{"name":"DARIAH-EU","altnm":"dariah-eu","urlp":"https://www.dariah.eu/","imgsrcp":"/static/logos/DARIAH-EU_logo.png","description":"Our Founders."}]

/***/ }),
/* 150 */
/***/ (function(module, exports) {

module.exports = [{"firstname":"Sara","surname":"Schulthess","position":"DH Scientist","description":"Multi-lingual translator, encoder and  ","pix":"/static/team/sara_grey.png"},{"firstname":"Martial","surname":"Sankar","position":"Lead Soft. developer / Data scientist","description":"Family man, basket-ball player & correlation thinker.","pix":"/static/team/martial_greyscale2.png"},{"firstname":"Mina","surname":"Monier","position":"Post-doctoral fellow in Humanities (Start:01.04.19)","description":"","pix":"/static/team/MinaMonier_1.jpg"},{"firstname":"Claire","surname":"Clivaz","position":"Head of DH+","description":"Associated senior scientist @ Vital-IT, theologist and interdisciplinarity enthusiast.","pix":"/static/team/clivaz_grey.png"}]

/***/ }),
/* 151 */
/***/ (function(module, exports) {

module.exports = {"2015":["Clivaz, Claire, Simon Mimouni and Bernard Pouderon. <i>Les judaïsmes dans tous leurs états aux Ier-IIIe siècles (Les Judéens des synagogues, les chrétiens et les rabbins). Actes du colloque de Lausanne 12-14 décembre 2012</i> (JAOC 5). Turnhout: Brepols, 2015.","Bornet, Philippe, Claire Clivaz, Nicole Durisch Gauthier and Étienne Honoré. <i>L’homme augmenté</i>. Vital-DH/Swiss Institute of Bioinformatics, 2015. https://etalk2.vital-it.ch/homme-augmente/","Clivaz, Claire. “En quête des couvertures et corpus. Quelques éclats d’humanités digitales.” In <i>Le Tournant numérique des Sciences humaines et sociales</i>, edited by Valérie Carayol and Franc Morandi, 97-109. Pessac: Maison des Sciences de l’Homme d’Aquitaine, 2015.","Clivaz, Claire. “Jésus était-il un bel homme ?” In <i>Jésus - 12 questions impertinentes</i>, edited by Gilles Bourquin, 43-44. Orbe: OPEC et Olivétan, 2015.","Clivaz, Claire. <i>Mais où est le corps ? L’Homme augmenté comme lieu des Humanities Digitales. L’Homme Augmenté</i>. Lausanne: VITAL-DH/Swiss Institute of Bioinformatics, 2015. http://etalk2.vital-it.ch/?dir=Clivaz#0","Clivaz, Claire. “Pratiques de lecture, identités et prise de conscience : la question des miniatures et du Biblaridion d’Apocalypse 10, 2.9-10.” In <i>Les judaïsmes dans tous leurs états aux Ier-IIIe siècles (Les Judéens des synagogues, les chrétiens et les rabbins). Actes du colloque de Lausanne 12-14 décembre 2012</i> (JAOC 5), edited by Claire Clivaz, Simon Mimouni and Bernard Pouderon, 321-344. Turnhout: Brepols, 2015.","Clivaz, Claire. “Trois éléments du dossier sans fin des sources et réécritures d'1 Cor 2,9.” In <i>La littérature apocryphe chrétienne et les Écritures juives</i>, edited by Rémi Gounelle and Bernard Mounier, 439-458. Prangins: Zèbre, 2015.","Clivaz, Claire, Cécile Pache, Marion Rivoal and Martial Sankar. “Multimodal Literacies and Academic Publishing: the eTalks.” <i>Information Services & Use</i> 35/4 (2015): 251-258. https://content.iospress.com/articles/information-services-and-use/isu781","Clivaz, Claire, Marion Rivoal and Martial Sankar. “A New Plateform for Editing Digital Multimedia: The eTalks.” In <i>New Avenues for Electronic Publishing in the Age of Infinite Collections and Citizen Science: Scale, Openness and Trust</i>, edited by Birgit Schmidt and Milena Dobreva, 156-159. IOS Press, 2015. http://ebooks.iospress.nl/publication/40894","Clivaz, Claire, and Sara Schulthess, “On the Source and Rewriting of 1 Corinthians 2.9 in Christian, Jewish and Islamic Traditions (1 Clem 34.8; GosJud 47.10–13; <i>a ḥadīth qudsī</i>).” <i>New Testament Studies</i> 61/2 (2015): 183–200. http://journals.cambridge.org/article_S0028688514000307","Rosenthaler, Lukas, Peter Fornaro and Claire Clivaz, “DASCH: Data and Service Center for the Humanities.” <i>Digital Scholarship in the Humanities</i> 30, suppl. 1 (2015): 43-49. https://academic.oup.com/dsh/article/30/suppl_1/i43/365238","Schulthess, Sara. “<i>Arabica sunt, non leguntur...</i> The Bible in Arabic: The Evidence of the Manuscripts (Leuven, 22-24 April 2015). Conference Report.” Comparative Oriental Manuscript Studies Bulletin 1/2 (2015): 125-126. https://www.aai.uni-hamburg.de/en/comst/pdf/bulletin1/pp125-126.pdf","Terras, Melissa, Claire Clivaz, D. Verhoeven and Frédécic Kaplan, eds. Special Issue “Digital Humanities 2014”. <i>Digital Scholarship in the Humanities</i> 30, suppl. 1 (2015). https://academic.oup.com/dsh/issue/30/suppl_1","Touati, Charlotte, and Claire Clivaz. “Apocryphal Texts about Other Characters in the Canoncial Gospels.” In <i>Oxford Handbook of Early Christian Apocrypha</i>, edited by Andrew Gregory, Tobias Nicklas, Christopher M. Tuckett, and Joseph Verheyden, 48-64. Oxford: Oxford University Press, 2015."],"2016":["Clivaz, Claire, Paul Dilley and David Hamidović, eds., in collaboration with Apolline Thromas. <i>Ancient Worlds in Digital Cultures</i> (DBS 1). Leiden: Brill, 2016. https://doi.org/10.1163/9789004325234","Schulthess, Sara. <i>Les manuscrits arabes des lettres de Paul. La reprise d'un champ de recherche négligé</i>. Doctoral thesis (Radboud University Nijmegen/Université de Lausanne), 2016. http://hdl.handle.net/2066/159141","Clivaz, Claire. “Categories of Ancient Christian Texts and Writing Materials: ‘Taking Once again a Fresh Starting Point’.” In <i>Ancient Worlds in Digital Cultures</i> (DBS 1), edited by Claire Clivaz, Paul Dilley and David Hamidović, in collaboration with Apolline Thromas, 35-58. Leiden: Brill, 2016. https://doi.org/10.1163/9789004325234_004","Clivaz, Claire. “Covers and Corpus Wanted! Some Digital Humanities Fragments.” <i>Digital Humanities Quarterly</i>, 10 (2016). http://digitalhumanities.org/dhq/vol/10/3/000257/000257.html","Clivaz, Claire. “De ‘numérique’ à ‘digital’.” <i>Études digitales</i>, 1 (2016): 255-256. https://doi.org/10.15122/isbn.978-2-406-06193-9.p.0255","Clivaz, Claire. “Introduction to ‘Digital Humanities in Ancient Jewish, Christian and Arabic Traditions’.” <i>Journal of Religion, Media and Digital Culture</i>, 5:1-20 (2016). https://doi.org/10.1163/21659214-90000068","Clivaz, Claire, Paul Dilley, David Hamidović, Mladen Popović, Caroline T. Schroeder and Joseph Verheyden, eds. Special Issue ”Digital Humanities in Ancient Jewish, Christian and Arabic Tradition.” <i>Journal of Religion, Media and Digital Culture</i> 5 (2016). https://doi.org/10.1163/21659214-90000068","Schulthess, Sara. “<i>Taḥrīf</i> in the Digital Age Ancient Worlds in a Digital Culture.” In <i>Ancient Worlds in Digital Cultures</i> (DBS 1), edited by Claire Clivaz, Paul Dilley and David Hamidović, in collaboration with Apolline Thromas, 214-230. Leiden: Brill, 2016. https://doi.org/10.1163/9789004325234_012"],"2017":["Clivaz, Claire. “Donner écho au discours de Ratisbonne du pape Benoît XVI.” <i>Cortile dei gentile</i> 10 March 2017. https://www.cortiledeigentili.com/donner-echo-au-discours-de-ratisbonne-du-pape-benoit-xvi-2006/","Clivaz, Claire. “Die Bibel im digitalen Zeitalter: Multimodale Schriften.” <I>Gemeinschaften Zeitschrift für Neues Testament</I> 20/39-40 (2017): 35-57. http://periodicals.narr.de/index.php/znt/article/view/3213","Clivaz, Claire. “Lost in Translation? The Odyssey of ‘Digital Humanities’ in French.” <i>Studia UBB Digitalia</i> 62/1 (2017): 26-41. http://digihubb.centre.ubbcluj.ro/journal/index.php/digitalia/article/view/4/18","Clivaz, Claire. “Multimodal Literacies and Continuous Data Publishing: une question de rythme.” In <i>Advances in Digital Scholarly Editing, Papers Presented at the DiXiT Conferences in The Hague, Cologne and Antwerp</i>, edited by Peter Boot et al., 99-104. Sidestone Press, 2017. https://www.sidestone.com/books/advances-in-digital-scholarly-editing","Clivaz, Claire, Sara Schulthess and Anastasia Chasapi. “HumaReC: Continuous Data Publishing in the Humanities.” <i>ERCIM News</i> 111 (2017): 21-22. https://ercim-news.ercim.eu/images/stories/EN111/EN111-web.pdf","Clivaz, Claire, Sara Schulthess and Martial Sankar. “Editing New Testament Arabic Manuscripts in a TEI-base: Fostering Close Reading in Digital Humanities.” Special Issue on Computer-Aided Processing of Intertextuality in Ancient Languages. <i>Journal of Data Mining & Digital Humanities</i> (2017): 1-6. https://jdmdh.episciences.org/paper/view?id=3700"],"2018":["Berra, Aurélien, Claire Clivaz, Sophie Marcotte and Emmanuelle Morlock, eds. French Language Special Issue (with the Spanish Language Special Issue). <i>Digital Humanities Quarterly</i> 12/1 (2018).  http://www.digitalhumanities.org/dhq/","Berra, Aurélien, Claire Clivaz, Sophie Marcotte and Emmanuelle Morlock. “Introduction.” French Language Special Issue (with the Spanish Language Special Issue). <i>Digital Humanities Quarterly</i> 12/1 (2018): §1-13. http://www.digitalhumanities.org/dhq/vol/12/1/000366/000366.html","Clivaz, Claire. “Transmettre les manuscrits bibliques dans l'Antiquité.” In <i>La Bible. Une encyclopédie contemporaine</i> (Le Monde de la Bible), Philippe Abadie et al., 199-205. Montrouge: Bayard Presse, 2018 (second edition).","Clivaz, Claire. “Thinking About the ‘Mind’ in Digital Humanities: Apple, Turing and Lovelace.” In <i>Information Technology in the Humanities. International Scientific and Practical Conference. Krasnoyarsk, 18-22 Septembre 2018</i>, 5-14. Krasnoyarsk: SFU, 2018. http://lib3.sfu-kras.ru/ft/LIB2/ELIB/b71/free/i-838981639.pdf","Clivaz, Claire. “Review of Jeffrey Siker, <i>Liquid Scripture: The Bible in a Digital World</i>.” <i>Review of Biblical Literature </i> 5 (2018): 1-6. https://www.bookreviews.org/bookdetail.asp?TitleId=11851","Schulthess, Sara. “Vaticanus Arabicus 13: What Do We Really Know About the Manuscript? With an Additional Note on the Ending of Mark.” Special issue <i>Arabica sunt, non leguntur... Five Studies on the Arabic Versions of the Bible in Jewish, Christian and Islamic Tradition</i> edited by Sara Schulthess, Herman Teule and Joseph Verheyden). <i>Journal of Eastern Christian Studies</i> 70,1–2 (2018): 63–84.","Schulthess, Sara. “Bible, 15: Arabic Versions.” In <i>Brill Encyclopedia of Early Christianity Online</i>, edited by David G. Hunter, Paul J.J. van Geest and Bert Jan Lietaert Peerbolte. Leiden: Brill, 2018. http://dx.doi.org/10.1163/2589-7993_EECO_COM_036571"],"2019":["Clivaz, Claire. <i>Écritures digitales. Digital Writing, Digital Scriptures</i> (DBS 4). Leiden: Brill, 2019. https://brill.com/view/title/54748.","Frey, Jörg, Claire Clivaz and Tobias Nicklas, eds., in collaboration with Jörg Röder. <i>Between Canonical and Apocryphal Texts</i> (WUNT I 419). Tübingen: Mohr Siebeck, 2019.  https://www.mohrsiebeck.com/buch/between-canonical-and-apocryphal-texts-9783161539275.","Hamidović, David, Claire Clivaz and Sarah Bowen Savant, eds., with Alessandra Marguerat. <i>Ancient Manuscripts in Digital Culture. Visualisation, Data Mining, Communication</i> (DBS 3). Leiden: Brill, 2019. https://brill.com/downloadpdf/title/34930.","Schulthess, Sara. <i>Les manuscrits arabes des lettres de Paul. État de la question et étude de cas (1 Corinthiens dans le Vat. Ar. 13)</i> (Biblia Arabica 6). Leiden, Boston: Brill, 2019. https://brill.com/view/title/38948","Clivaz, Claire. “‘(According) To the Hebrews’. An Apocryphal Gospel and a Canonical Letter Read in Egypt.” In <i>Between Canonical and Apocryphal Texts</i> (WUNT I 419), edited by Jörg Frey, Claire Clivaz and Tobias Nicklas, in collaboration with Jörg Röder, 261-277. Tübingen: Mohr Siebeck, 2019. https://www.mohrsiebeck.com/buch/between-canonical-and-apocryphal-texts-9783161539275.","Monier, Monier. “GA 304, Theophylact’s Commentary and the Ending of Mark.” <i>Filología Neotestamentaria</i> 32 (2019): 9-22.","Schulthess, Sara. “The Bible in Arabic: Digital Resources and Future Challenges.” <i>Open Theology</i> 5/1 (2019): 217–226. https://doi.org/10.1515/opth-2019-0016","Clivaz, Claire. “The Impact of Digital Research: Thinking about the MARK16 Project.” <i>Open Theology</i> 5/1 (2019): 1-12. https://doi.org/10.1515/opth-2019-0001","Clivaz, Claire, and Bowen Savant Sarah. “The Dissemination of the Digital Humanities within Research on Biblical, Early Jewish and Christian Studies.” In <i>Ancient Manuscripts in Digital Culture. Visualisation, Data Mining, Communication</i> (DBS 3), edited by David Hamidović, Claire Clivaz and Sarah Bowen Savant, with Alessandra Marguerat, 1-12. Leiden: Brill, 2019. https://brill.com/view/book/edcoll/9789004399297/BP000001.xml.","Schulthess, Sara, Anastasia Chasapi, Ioannis Xenarios, Martial Sankar and Clivaz Claire. “HumaReC Project: Digital New Testament and Continuous Data Publishing (Poster)”, <i>DH2017 Proceedings</i>, edited by Diane Jakacki, Stefan Sinclair and Michael Sinatra. Montreal: Erudit, in press.","Clivaz, Claire. “Returning to Mark 16,8 : What’s New?” <i>Ephemerides Theologicae Lovanienses</i> 96/4 (2019), in press."]}

/***/ }),
/* 152 */
/***/ (function(module, exports) {

module.exports = ["Edition","Etalks","Reproducibility","Intellectual Property","Copyright","Web development","Big data","Manuscript Transcription","Multilingual","Experiments","Strategy","Technology"]

/***/ }),
/* 153 */
/***/ (function(module, exports) {

module.exports = ["All Products","Curation","Viewer","Edition","Manuscript","Portal","Mining","Publishing"]

/***/ })
],[43]);
//# sourceMappingURL=app.037d077b456cf2e5dd34.js.map